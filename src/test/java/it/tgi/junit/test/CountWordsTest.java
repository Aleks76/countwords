package it.tgi.junit.test;

import it.tgi.plugin.test.CountWordsPlugin;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * Created by Alessandro on 13/03/2017.
 */
@SuppressWarnings("DefaultFileTemplate")
public class CountWordsTest {

    private static final String TEST_FILE = "D:\\Projects\\intelliJ_prj\\PluginDemo\\CountWords\\src\\main\\java\\it\\tgi\\plugin\\test\\CountWordsPlugin.java";

    /**
    * Verify the number of words of the class file CountWordsPlugin.java
    * NOTE: Must be modifiy the value if you modify the inner file
    */
    @Test
    public void testCountFile() {
        CountWordsPlugin p = new CountWordsPlugin();
        Long result = p.execute(new File(TEST_FILE));
        assertEquals("The file contains 360 words", new Long(360),result);
    }

    /**
    * Verify the number of words in a String
    */
   @Test
    public void testCountString() {
       CountWordsPlugin p = new CountWordsPlugin();
       Long result =p.execute("test to count the number of words in this string");
       assertEquals("The String contains 10 words", new Long(10), result);
    }

}

package it.tgi.plugin.test;

import org.plugface.PlugfaceContext;
import org.plugface.Plugin;
import org.plugface.PluginConfiguration;
import org.plugface.PluginStatus;
import org.plugface.impl.DefaultPluginConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.StringTokenizer;

/**
 * Created by Alessandro on 10/03/2017.
 */
public class CountWordsPlugin implements Plugin<Object, Long> {

    private static final String DELIMITERS = " -.,:;!?|_";
    private static final Logger LOG = LoggerFactory.getLogger(CountWordsPlugin.class);
    private final String name;
    private PluginConfiguration pConfiguration;
    private PlugfaceContext pContext;
    private PluginStatus pStatus;

    public CountWordsPlugin() {
        this.name ="countWords";
        this.pConfiguration = new DefaultPluginConfiguration();
        this.pStatus = PluginStatus.DISABLED;
    }

    public void start() {}

    public void stop() {}

    public void enable() {}

    public void disable() {}

    public boolean isEnabled() {
        return false;
    }

    /**
     * This method return the number of words of a String or File Object
     * */
    public Long execute(Object object) {

        if (object instanceof File)
            return executeFile((File) object);
        else if (object instanceof String)
            return executeString((String) object);
        else
            return null;
    }

    /**
     * This method return the number of words in a txt file
     * */
    private Long executeFile(File fileToRead) {

        String line;
        long wordCounter=0;

        try{
            FileReader fileStream = new FileReader( fileToRead );
            BufferedReader bufferedReader = new BufferedReader( fileStream );

            while ((line=bufferedReader.readLine())!=null)
                wordCounter += executeString(line);


        } catch ( FileNotFoundException ex ) {
            LOG.error("File not found: {}", fileToRead);
        } catch ( IOException ex ) {
            LOG.error("There is an error: {}", ex);
        }
        LOG.debug("Plugin: " + this.name.toUpperCase() + " --> The file contains " + wordCounter + " words.");

        return wordCounter;
    }

    /**
     * This method return the number of words of a String
     * */
    private Long executeString (String text) {
        return Long.valueOf(new StringTokenizer(text, DELIMITERS).countTokens());
    }

    public String getName() {
        return this.name;
    }

    public void setName(String s) {}

    public PluginStatus getStatus() {
        return this.pStatus;
    }

    public void setStatus(PluginStatus pluginStatus) {
        this.pStatus = pluginStatus;
    }

    public PluginConfiguration getPluginConfiguration() {
        return this.pConfiguration;
    }

    public void setPluginConfiguration(PluginConfiguration pluginConfiguration) {
        this.pConfiguration = pluginConfiguration;
    }

    public PlugfaceContext getContext() {
        return this.pContext;
    }

    public void setContext(PlugfaceContext plugfaceContext) {
        this.pContext = plugfaceContext;
    }
}
